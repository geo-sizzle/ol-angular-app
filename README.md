# OpenLayers 6 & Angular 13 boilerplate

[![TypeScript version][ts-badge]][typescript-4-5]
[![Angular version][angular-badge]][angular-13-1-x]
[![OpenLayes version][ol-badge]][ol-6-10-x]

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


[ts-badge]: https://img.shields.io/badge/TypeScript-4.5-blue.svg
[typescript-4-5]: https://www.typescriptlang.org/docs/handbook/release-notes/typescript-4-5.html
[angular-badge]: https://img.shields.io/badge/Angular->=%2013.1.2-red.svg 
[angular-13-1-x]: https://github.com/angular/angular/blob/13.1.x/CHANGELOG.md
[ol-badge]: https://img.shields.io/badge/OpenLayers->=%206.10-00525D.svg
[ol-6-10-x]: https://github.com/openlayers/openlayers/releases/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayerMenuModule } from './layer-menu/layer-menu.module';

@NgModule({
  imports: [
    CommonModule,
    LayerMenuModule
  ],
  declarations: [],
  exports: [
    LayerMenuModule
  ]
})
export class SharedModule { }

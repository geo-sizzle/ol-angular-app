import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeoViewerComponent } from './geo-viewer.component';
import { MapService } from './map.service';
import { initViewerConfigs, LayerConfig, VIEWER_CONFIG } from './geo-viewer-config';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    GeoViewerComponent
  ],
  exports: [
    GeoViewerComponent
  ]
})
export class GeoViewerModule {
  static forRoot(configCallback: () => Array<LayerConfig>) {
    return {
      ngModule: GeoViewerModule,
      providers: [
        MapService,
        {
          provide: 'CONFIG_GEO_VIEWER_CALLBACK',
          useValue: configCallback
        }, {
          provide: VIEWER_CONFIG,
          useFactory: initViewerConfigs,
          deps: ['CONFIG_GEO_VIEWER_CALLBACK']
        }
      ]
    }
  }
}

import { InjectionToken } from '@angular/core';

export enum LayerName {
    OSM = 'osm',
    BING = 'bing',
    HikingMap = 'hiking-map'
}

export interface LayerConfig {
    name: LayerName;
    visible: boolean;
}

export const VIEWER_CONFIG = new InjectionToken<Array<LayerConfig>>('VIEWER_CONFIG');

export function initViewerConfigs(configCallback: () => Array<LayerConfig>) {
    return configCallback();
}

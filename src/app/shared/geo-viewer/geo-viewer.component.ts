import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { Map, MapBrowserEvent, View } from 'ol';
import { BingMaps, OSM, XYZ } from 'ol/source';
import { LayerConfig, LayerName, VIEWER_CONFIG } from './geo-viewer-config';
import { MapService } from './map.service';

@Component({
  selector: 'geo-viewer',
  templateUrl: './geo-viewer.component.html',
  styleUrls: ['./geo-viewer.component.scss']
})
export class GeoViewerComponent implements OnInit {
  @ViewChild('mapHolder', { static: true }) mapRef!: ElementRef;

  constructor(
    @Inject(VIEWER_CONFIG) private layerConfigs: Array<LayerConfig>,
    private mapService: MapService
  ) { }

  ngOnInit() {
    this.mapService.map = new Map({
      target: this.mapRef.nativeElement,
      view: new View({
        // mecator default coordinate's midden (nl)
        center: [634049.16, 6867228.70],
        zoom: 9
      }),
    });

    // Open street map base layer ...
    this.layerConfigs.forEach((config: LayerConfig) => {

      switch (config.name) {
        case LayerName.OSM: {
          this.mapService.map.addLayer(this.mapService.tileLayerFactory<OSM>({
            name: LayerName.OSM,
            title: 'OpenStreetMap',
            source: new OSM(),
            baseLayer: true,
            visible: config.visible,
          }));
        } break;

        // add hiking map ...
        case LayerName.HikingMap: {
          this.mapService.map.addLayer(this.mapService.tileLayerFactory<XYZ>({
            name: LayerName.HikingMap,
            title: 'Hike & Bike',
            source: new XYZ({
              url: 'https://tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png'
            }),
            baseLayer: true,
            visible: config.visible
          }));
        } break;

        // Open street map base layer ...
        case LayerName.BING: {
          this.mapService.map.addLayer(this.mapService.tileLayerFactory<BingMaps>({
            name: LayerName.BING,
            title: 'Bing',
            source: new BingMaps({
              key: 'AiNZC3-YIsJ-1uIH0ShGFjj8wmISzP6PwclHqiIxkSgvDWrg_HoGum0ehTNAJyES',
              imagerySet: 'AerialWithLabelsOnDemand',
            }),
            baseLayer: true,
            visible: config.visible
          }));
        } break;
      }
    });

    // initialize ol map click listener ...
    this.mapService.map.on('click', (e: MapBrowserEvent<any>) => this.mapService.notifyMapClick(e));
  }
}

import { Injectable } from '@angular/core';
import { Map, MapBrowserEvent } from 'ol';
import { Subject, Observable } from 'rxjs';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';
import { Source } from 'ol/source';
import VectorSource from "ol/source/Vector";
import { StyleLike } from "ol/style/Style";
import { Tile as TileSource } from 'ol/source';
import { LayerName } from './geo-viewer-config';
import { Helpers } from '@core';
import BaseLayer from 'ol/layer/Base';
import { TileWMS } from 'ol/source';
import { Geometry } from 'ol/geom';

export interface SourceOption<S extends Source> {
  title: string;
  name: LayerName;
  source: S;
  visible: boolean;
  baseLayer?: boolean;
  style?: StyleLike
}

@Injectable({
  providedIn: 'root'
})
export class MapService {

  public mapClick!: Observable<MapBrowserEvent<UIEvent>>;

  private mapClickSubject!: Subject<MapBrowserEvent<UIEvent>>;

  private mapRef!: Map;

  constructor() {
    this.mapClickSubject = new Subject<MapBrowserEvent<UIEvent>>();
    this.mapClick = this.mapClickSubject.asObservable();
  }

  get map(): Map {
    return this.mapRef;
  }
  set map(map: Map) {
    this.mapRef = map;
  }

  public notifyMapClick(e: MapBrowserEvent<UIEvent>): void {
    this.mapClickSubject.next(e);
  }

  /**
   * 
   * @param source
   */
  public vectorLayerFactory<Source extends VectorSource<Geometry>>(source: SourceOption<Source>): VectorLayer<Source> {
    const layer = new VectorLayer({
      visible: source.visible,
      source: source.source,
      style: source.style
    });
    layer.setProperties({
      id: Helpers.generateUid(),
      name: source.name,
      title: source.title,
      baseLayer: source.baseLayer !== undefined ? source.baseLayer : false
    });
    return layer;
  }

  /**
   * 
   * @param source
   * @returns 
   */
  public tileLayerFactory<Source extends TileSource>(source: SourceOption<Source>): TileLayer<Source> {
    const layer = new TileLayer({
      visible: source.visible,
      source: source.source
    });
    layer.setProperties({
      id: Helpers.generateUid(),
      name: source.name,
      title: source.title,
      baseLayer: source.baseLayer !== undefined ? source.baseLayer : false
    });
    return layer;
  }

  public getAllLayers(): BaseLayer[] {
    return this.map.getLayers().getArray();
  }

  public getBaseLayers(): TileLayer<TileWMS>[] {
    // console.log(this.map.getLayers().getArray());
    return this.map.getLayers().getArray().filter(l => l.get('baseLayer')) as TileLayer<TileWMS>[];
  }
}

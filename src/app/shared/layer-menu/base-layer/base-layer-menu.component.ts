import { Component, OnInit } from '@angular/core';
import { LayerName, MapService } from '@geo-viewer';
import { Tile as TileLayer } from 'ol/layer';
import { TileWMS } from 'ol/source';

interface LayerItem {
  id: string;
  name: LayerName;
  title: string;
  visible: boolean;
  layer: TileLayer<TileWMS>;
}

@Component({
  selector: 'base-layer-menu',
  templateUrl: './base-layer-menu.component.html',
  styleUrls: ['./base-layer-menu.component.scss']
})
export class BaseLayerMenuComponent implements OnInit {

  public layerItems = new Array<LayerItem>();

  constructor(private mapService: MapService) { }

  ngOnInit() {
    this.mapService.getBaseLayers().forEach((layer: TileLayer<TileWMS>): void => {
      // console.log(layer.getVisible());
      this.layerItems.push({
        id: layer.get('id'),
        name: layer.get('name'),
        title: layer.get('title'),
        visible: layer.getVisible(),
        layer
      });
    });
  }

  public onSwitch(item: LayerItem): void {
    // turn off ...
    const currentActive = this.layerItems.find((l: LayerItem) => l.visible);
    if (currentActive) {
      currentActive.visible = false
      currentActive.layer.setVisible(false);
    }
    // turn on ...
    item.visible = true
    item.layer.setVisible(true);
    // console.log(item);
  }

  public trackById(index: number, item: LayerItem): string { 
    return item.id;
  };
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseLayerMenuComponent } from './base-layer/base-layer-menu.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseLayerMenuComponent
  ],
  exports: [
    BaseLayerMenuComponent
  ]
})
export class LayerMenuModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LayerName, GeoViewerModule } from '@geo-viewer';
import { SharedModule } from '@shared';

@NgModule({
  imports: [
    BrowserModule,
    SharedModule,
    GeoViewerModule.forRoot(() => {
      return [{
          name: LayerName.BING,
          visible: false,
        }, {
          name: LayerName.HikingMap,
          visible: false,
        }, {
          name: LayerName.OSM,
          visible: true,
        }]
    })
  ],
  declarations: [
    AppComponent
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
